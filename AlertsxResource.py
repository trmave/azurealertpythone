import json
import matplotlib.pyplot as plt
from collections import Counter

# Loading data from the JSON file
with open('alert_details.json') as file:
    data = json.load(file)

# Extracting resources and counting alerts for each
resource_counts = Counter()
for subscription in data['subscriptions']:
    for alert in subscription['alerts']:
        resource = alert['Target Resource']
        resource_counts[resource] += 1

# Preparing data for the graph
resources, counts = zip(*resource_counts.items())

# Creating the graph
plt.figure(figsize=(20, 12))
plt.barh(resources, counts, color='skyblue')
plt.xlabel('Number of Alerts')
plt.ylabel('Target Resource')
plt.title('Number of Alerts per Resource')
plt.tight_layout()

# Code to save the graph
plt.savefig('alerts_per_resource_v2.png')
