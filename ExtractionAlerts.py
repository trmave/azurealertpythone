from azure.identity import DefaultAzureCredential
from azure.mgmt.alertsmanagement import AlertsManagementClient
from azure.mgmt.alertsmanagement.models import TimeRange
from azure.mgmt.resource.subscriptions import SubscriptionClient
import azure.core.exceptions
import json

def get_filtered_subscriptions(subscription_client, filter_names=None):
    all_subscriptions = subscription_client.subscriptions.list()
    return {sub.display_name: sub.subscription_id for sub in all_subscriptions if not filter_names or sub.display_name in filter_names}

def add_alert_details(alert, subscription_name):
    essentials = alert.properties.essentials
    details = {
        "Alert ID": alert.id,
        "Severity": essentials.severity,
        "Alert State": essentials.alert_state,
        "Description": essentials.additional_properties.get('description', 'Not available'),
        "Signal Type": essentials.signal_type,
        "Target Resource": essentials.target_resource,
        "Target Resource Name": essentials.target_resource_name,
        "Target Resource Group": essentials.target_resource_group,
        "Target Resource Type": essentials.target_resource_type,
        "Monitoring Service": essentials.monitor_service,
        "Alert Rule": essentials.alert_rule,
        "Start Date Time": essentials.start_date_time.isoformat() if essentials.start_date_time else None,
        "Last Modified Date Time": essentials.last_modified_date_time.isoformat() if essentials.last_modified_date_time else None,
        "Time When Alert Condition Was Resolved": essentials.monitor_condition_resolved_date_time.isoformat() if essentials.monitor_condition_resolved_date_time else None,
        "Last Modified By User": essentials.last_modified_user_name
    }
    alert_details.setdefault(subscription_name, []).append(details)

# Authentication with Azure
credential = DefaultAzureCredential()
subscription_client = SubscriptionClient(credential)

# Data structures to store information
alert_summary = {}
alert_details = {}
total_global_alerts = 0
global_severities = {"Sev0": 0, "Sev1": 0, "Sev2": 0, "Sev3": 0, "Sev4": 0}

# Subscription selection mode
selection_mode = input("Type 'all' to select all subscriptions or 'specific' to select some: ").lower()
while selection_mode not in ['all', 'specific']:
    selection_mode = input("Invalid input. Type 'all' or 'specific': ").lower()

if selection_mode == 'specific':
    subscription_names = [
        "Company1 - System1",
        "Company1 - Europe Production",
        "Company1 - Global Production",
        "Company2 - EMEA Production",
        "Partner1 - Operations Center",
        "GreenCloud - Production",
        "Company2 - AMER Production",
        "Company2 - System1 - Production",
        "Partner1 - Production"
    ]

    filtered_subscriptions = get_filtered_subscriptions(subscription_client, subscription_names)
else:
    filtered_subscriptions = get_filtered_subscriptions(subscription_client)

for sub_name, sub_id in filtered_subscriptions.items():
    try:
        print(f"Processing alerts for subscription: {sub_name}")
        client = AlertsManagementClient(credential, sub_id)
        time_range = TimeRange("1h")
        alerts = client.alerts.get_all(time_range=time_range)

        severities = {"Sev0": 0, "Sev1": 0, "Sev2": 0, "Sev3": 0, "Sev4": 0}
        for alert in alerts:
            severity = alert.properties.essentials.severity
            severities[severity] = severities.get(severity, 0) + 1
            total_global_alerts += 1
            global_severities[severity] = global_severities.get(severity, 0) + 1
            add_alert_details(alert, sub_name)

        alert_summary[sub_name] = {
            "Total Alerts": sum(severities.values()),
            "Summary by Severity": severities
        }

    except azure.core.exceptions.HttpResponseError as e:
        print(f"Could not process alerts for subscription {sub_name}: {e.message}")


# Prepare final structure for JSON file
final_structure = {"subscriptions": []}
for subscription, alerts in alert_details.items():
    final_structure["subscriptions"].append({
        "subscription_name": subscription,
        "alerts": alerts
    })

# Serialize and write to JSON files
with open('alert_summary.json', 'w') as file:
    json.dump(alert_summary, file, indent=4)

with open('alert_details.json', 'w') as file:
    json.dump(final_structure, file, indent=4)

with open('total_alerts.json', 'w') as file:
    json.dump({"Total Global Alerts": total_global_alerts, "Global Summary by Severity": global_severities}, file, indent=4)
