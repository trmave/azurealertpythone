# Azure Alert Manager & Visualizer

## Description

This project offers a comprehensive solution for the management and visualization of alerts in Microsoft Azure. Using Python, the `Extracciondealertas` script automates the collection of alerts from various Azure subscriptions, categorizes them by severity, and stores them for detailed analysis. Additionally, the project includes a visualization tool, `AlertasxRecurso`, that generates intuitive graphs, allowing users to quickly identify the resources most affected by the alerts.

## Features

- **Alert Collection Automation:** Gathers alerts from multiple Azure subscriptions using `Extracciondealertas`.
- **Classification and Analysis:** Categorizes alerts by severity and other criteria.
- **Data Visualization:** Generates bar graphs to visualize the frequency of alerts per resource with `AlertasxRecurso`.
- **User-friendly Interface:** Simple user interface for quick setup and operation.

## Installation

To install and run this project, follow these steps:

```bash
git clone https://gitlab.com/trmave/AzureAlertPython.git
cd AzureAlertPython
# Make sure you have Python installed
# Install the necessary dependencies
pip install azure-identity azure-mgmt-alertsmanagement matplotlib
python Extracciondealertas.py
# Here you choose if you want all your subscriptions that you can use in your Azure account or from a list that you choose within the file, there are two options, all or specific
python AlertasxRecurso.py 
# This will generate a graph
